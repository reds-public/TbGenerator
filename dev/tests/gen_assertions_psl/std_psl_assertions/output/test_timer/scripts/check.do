
proc compile_duv { } {
    # Compile with the psl file
    vcom -pslfile ../src_tb/timer_top_assertions.psl ../../../../../src_vhd/timer_top.vhd -work work    -suppress 7033     -2002

}

proc check_psl {} {

    formal compile -d timer_top  -work work

    formal verify
}

compile_duv

check_psl 
