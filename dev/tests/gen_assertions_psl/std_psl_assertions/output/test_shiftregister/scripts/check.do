
proc compile_duv { } {
    # Compile with the psl file
    vcom -pslfile ../src_tb/shiftregister_assertions.psl ../../../../../src_vhd/shiftregister.vhd -work work    -suppress 7033     -2002

}

proc check_psl {DATASIZE} {

    formal compile -d shiftregister -GDATASIZE=$DATASIZE -work work

    formal verify
}

compile_duv

check_psl 8 
