#!/bin/sh

SCRIPT=`realpath -s $0`
SCRIPTPATH=`dirname $SCRIPT`

mkdir $SCRIPTPATH/output

python3 $SCRIPTPATH/../../../src/gen_assertions_psl/std_psl_assertions/gen_std_psl_assertions.py -f -i $SCRIPTPATH/../../src_vhd/shiftregister.vhd -f -d $SCRIPTPATH/output/test_shiftregister
python3 $SCRIPTPATH/../../../src/gen_assertions_psl/std_psl_assertions/gen_std_psl_assertions.py -f -i $SCRIPTPATH/../../src_vhd/bcd_adder.vhd -f -d $SCRIPTPATH/output/test_bcd_adder
python3 $SCRIPTPATH/../../../src/gen_assertions_psl/std_psl_assertions/gen_std_psl_assertions.py -f -i $SCRIPTPATH/../../src_vhd/timer_top.vhd -f -d $SCRIPTPATH/output/test_timer
