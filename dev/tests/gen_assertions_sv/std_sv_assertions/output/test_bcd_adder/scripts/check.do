
proc compile_duv { } {

    vcom ../../../../../src_vhd/bcd_adder.vhd -work work    -suppress 7033     -2002

}

proc check_sva {ERRNO NDIGITS} {
    vlog ../src_tb/bcd_adder_assertions.sv  ../src_tb/bcd_adder_sv_wrapper.sv

    formal compile -d bcd_adder_sv_wrapper -GERRNO=$ERRNO -GNDIGITS=$NDIGITS -work work

    formal verify
}

compile_duv

check_sva 0 4 
