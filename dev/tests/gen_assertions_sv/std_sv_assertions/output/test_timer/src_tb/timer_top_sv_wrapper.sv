/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : timer_top_sv_wrapper.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module is a wrapper that binds the DUV with the
              module containing the assertions

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/

// The wrapper has no port
module timer_top_sv_wrapper ();

    // Instantiation of the DUV
    timer_top duv();

    // Binding of the DUV and the assertions module
    bind duv timer_top_assertions binded(.*);

endmodule

/*
The following works as well

// The wrapper has the same interface as the component to verify
module timer_top_sv_wrapper (
    input logic clock_i,
    input logic nReset_i,
    input logic Mono_nDiv_i,
    input logic en_div_i,
    input logic run_mono_i,
    input logic[6:0] val_i,
    output logic done_o
);

    // Instantiation of the DUV
    timer_top duv(.*);

    // Binding of the DUV and the assertions module
    bind duv timer_top_assertions binded(.*);

endmodule
*/
