/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : timer_top_assertions.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module will contain the assertions for verifying
              timer_top

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The module has the same interface as the component to verify except all
// ports are inputs
module timer_top_assertions (
    input logic clock_i,
    input logic nReset_i,
    input logic Mono_nDiv_i,
    input logic en_div_i,
    input logic run_mono_i,
    input logic[6:0] val_i,
    input logic done_o
    
);

    // TODO : Write your assertions

endmodule
