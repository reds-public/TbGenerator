/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : shiftregister_assertions.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module will contain the assertions for verifying
              shiftregister

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The module has the same interface as the component to verify except all
// ports are inputs
module shiftregister_assertions #(int DATASIZE = 8)(
    input logic clk_i,
    input logic rst_i,
    input logic[1:0] mode_i,
    input logic[DATASIZE - 1:0] load_value_i,
    input logic ser_in_msb_i,
    input logic ser_in_lsb_i,
    input logic[DATASIZE - 1:0] value_o
    
);

    // TODO : Write your assertions

endmodule
