--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : bcd_adder_tb.vhd
-- Author   : TbGenerator
-- Date     : 06.03.2019
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   06.03.2019  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity bcd_adder_tb is
    generic (
        TESTCASE : integer := 0;
        ERRNO    : integer := 0;
        NDIGITS  : integer := 4
    );
    
end bcd_adder_tb;

architecture testbench of bcd_adder_tb is

    constant CLK_PERIOD : time := 10 ns;

    signal input0_sti  : bcd_number(NDIGITS - 1 downto 0);
    signal input1_sti  : bcd_number(NDIGITS - 1 downto 0);
    signal result_obs  : bcd_number(NDIGITS downto 0);
    signal hamming_obs : std_logic_vector(integer(ceil(log2(real(4*NDIGITS)))) downto 0);
    
    signal sim_end_s : boolean := false;

    component bcd_adder is
    generic (
        ERRNO   : integer := 0;
        NDIGITS : integer := 4
    );
    port (
        input0_i  : in bcd_number(NDIGITS - 1 downto 0);
        input1_i  : in bcd_number(NDIGITS - 1 downto 0);
        result_o  : out bcd_number(NDIGITS downto 0);
        hamming_o : out std_logic_vector(integer(ceil(log2(real(4*NDIGITS)))) downto 0)
    );
    end component;
    

begin

    duv : bcd_adder
    generic map (
        ERRNO   => ERRNO,
        NDIGITS => NDIGITS
    )
    port map (
        input0_i  => input0_sti,
        input1_i  => input1_sti,
        result_o  => result_obs,
        hamming_o => hamming_obs
    );
    

    clk_proc : process is
    begin
        input0_sti <= '0';
        wait for CLK_PERIOD / 2;
        input0_sti <= '1';
        wait for CLK_PERIOD / 2;
        if sim_end_s then
            wait;
        end if;
    end process; -- clk_proc

    rst_proc : process is
    begin
        input0_sti <= '1';
        wait for 2 * CLK_PERIOD;
        input0_sti <= '0';
        wait;
    end process; -- rst_proc

    stimulus_proc: process is
    begin
        -- input0_sti  <= default_value;
        -- input1_sti  <= default_value;
        

        -- do something
        case TESTCASE is
            when 0      => -- default testcase
            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
