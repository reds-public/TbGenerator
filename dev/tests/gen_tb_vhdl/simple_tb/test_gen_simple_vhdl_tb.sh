#!/bin/sh

SCRIPT=`realpath -s $0`
SCRIPTPATH=`dirname $SCRIPT`

mkdir $SCRIPTPATH/output

python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/shiftregister.vhd -d $SCRIPTPATH/output/test_shiftregister
python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/bcd_adder.vhd -d $SCRIPTPATH/output/test_bcd_adder
python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/timer_top.vhd -d $SCRIPTPATH/output/test_timer
python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/alu.vhd -d $SCRIPTPATH/output/test_alu1 -t template_combi1
python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/alu.vhd -d $SCRIPTPATH/output/test_alu2 -t template_combi2
python3 $SCRIPTPATH/../../../src/gen_tb_vhdl/simple_tb/gen_simple_vhdl_tb.py -f -i $SCRIPTPATH/../../src_vhd/alu.vhd -d $SCRIPTPATH/output/test_alu3 -t template_combi3

cwd=$(pwd)

echo $SCRIPTPATH
rm output.txt
cd $SCRIPTPATH/output/test_shiftregister/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $SCRIPTPATH/output/test_bcd_adder/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $SCRIPTPATH/output/test_timer/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $SCRIPTPATH/output/test_alu1/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $SCRIPTPATH/output/test_alu2/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $SCRIPTPATH/output/test_alu3/comp
echo exit | vsim -c -do ../scripts/sim.do >> $SCRIPTPATH/output/output.txt

cd $cwd
