/*******************************************************************************
HEIG-VD
Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
School of Business and Engineering in Canton de Vaud
********************************************************************************
REDS Institute
Reconfigurable Embedded Digital Systems
********************************************************************************

File     : timer_top_tb.sv
Author   : TbGenerator
Date     : 06.03.2019

Context  :

********************************************************************************
Description : This module is a simple SystemVerilog testbench.
              It instanciates the DUV and proposes a testcase function.

********************************************************************************
Dependencies : -

********************************************************************************
Modifications :
Ver   Date        Person     Comments
0.1   06.03.2019  TbGen      Initial version

*******************************************************************************/


// The wrapper has the same interface as the component to verify
module timer_top_tb ();

    // Instantiation of the DUV
    timer_top duv(.*);

    logic clock_i;
    logic nReset_i;
    logic Mono_nDiv_i;
    logic en_div_i;
    logic run_mono_i;
    logic[6:0] val_i;
    logic done_o;
    


    // clock generation
    always #5 TODO__clock_notfound = ~TODO__clock_notfound;

    // Clocking block
    default clocking cb @(posedge TODO__clock_notfound);
        output #3ns TODO__reset_notfound,
               clock_i = clock_i,
               nReset_i = nReset_i,
               Mono_nDiv_i = Mono_nDiv_i,
               en_div_i = en_div_i,
               run_mono_i = run_mono_i,
               val_i = val_i;
        input 
               done_o = done_o;
    endclocking
    


    task testcase0();
        $display("Let's start first test case");
        // TODO : assign default values to inputs

        // Let's reset the system
        cb.TODO__reset_notfound <= 1;
        ##1 cb.TODO__reset_notfound <= 0;

        // Let's wait a bit
        ##10;

        repeat (10) begin
            // Let's generate some stimuli
            // TODO : generate some stimuli

            ##1;
        end
    endtask



    // Programme lancé au démarrage de la simulation
    program TestSuite;
        initial begin
            if (testcase == 0)
                testcase0();
            else
                $display("Ach, test case not yet implemented");
            $display("done!");
            $stop;
        end
    endprogram

endmodule
