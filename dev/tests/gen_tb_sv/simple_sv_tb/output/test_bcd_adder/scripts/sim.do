
!/usr/bin/tclsh

# Main proc at the end #

#------------------------------------------------------------------------------
proc compile_duv { } {
  global Path_DUV
  puts "\nSystemVerilog DUV compilation :"

  vlog $Path_DUV/bcd_adder.sv
}

#------------------------------------------------------------------------------
proc compile_tb { } {
  global Path_TB
  global Path_DUV
  puts "\nSystemVerilog TB compilation :"

  vlog +incdir+$Path_DUV $Path_TB/bcd_adder_tb.sv
}

#------------------------------------------------------------------------------
proc sim_start {TESTCASE ERRNO NDIGITS} {

  vsim -t 1ns -novopt -GERRNO=$ERRNO -GNDIGITS=$NDIGITS --GTESTCASE=$TESTCASE work.bcd_adder_tb
#  do wave.do
  add wave -r *
  wave refresh
  run -all
}

#------------------------------------------------------------------------------
proc do_all {TESTCASE ERRNO NDIGITS} {
  compile_duv
  compile_tb
  sim_start $TESTCASE $ERRNO $NDIGITS
}

## MAIN #######################################################################

# Compile folder ----------------------------------------------------
if {[file exists work] == 0} {
  vlib work
}

puts -nonewline "  Path_VHDL => "
set Path_DUV     "../src_sv"
set Path_TB       "../src_tb"

global Path_DUV
global Path_TB

# start of sequence -------------------------------------------------

if {$argc>0} {
  if {[string compare $1 "all"] == 0} {
    do_all 0 $2 $3 
  } elseif {[string compare $1 "comp_duv"] == 0} {
    compile_duv
  } elseif {[string compare $1 "comp_tb"] == 0} {
    compile_tb
  } elseif {[string compare $1 "sim"] == 0} {
    sim_start 0 $2
  }

} else {
  do_all 0 0 4 
}
