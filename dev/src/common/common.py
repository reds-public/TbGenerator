

import string, re
import time
import os


class MyTemplate(string.Template):
    delimiter = '$'
    pattern = r'''
    \$\{(?:
      (?P<escaped>\$) |   # Escape sequence of two delimiters
      (?P<named>[_a-z][_a-z0-9]*)      |   # delimiter and a Python identifier
      {(?P<braced>[_a-z][_a-z0-9]*)}   |   # delimiter and a braced identifier
      (?P<invalid>)              # Other ill-formed delimiter exprs
    )\}
    '''

# adapted from https://stackoverflow.com/questions/36739667/python-templates-for-generating-python-code-with-proper-multiline-indentation
def substitute(s, reps):
    t = MyTemplate(s)
#    t = string.Template(s)
    i=0; cr = {}  # prepare to iterate through the pattern string
    while True:
        # search for next replaceable token and its prefix
        m =re.search(r'^(.*?)\$\{(.*?)\}', s[i:], re.MULTILINE)
        if m is None: break  # no more : finished
        # the list is joined using the prefix if it contains only blanks
        sep = ('\n' + m.group(1)) if m.group(1).strip() == '' else '\n'
        augmentedString = reps[m.group(2)].replace('\n', sep)
        cr[m.group(2)] = augmentedString
        i += m.end()   # continue past last processed replaceable token
    return t.substitute(cr)  # we can now substitute


def generateFile(templateFilename, outputFilename, replacements, overwrite):
    if (not overwrite):
        if os.path.exists(outputFilename):
            print('The file {0} already exists. If you want to overwrite it, start the command with the -f option'.format(outputFilename))
            return
    fin = open(templateFilename, 'r')
    templateContent = fin.read()
    outputContent = substitute(templateContent, replacements)
    fout = open(outputFilename, 'w')
    fout.write(outputContent)
    fout.close()
