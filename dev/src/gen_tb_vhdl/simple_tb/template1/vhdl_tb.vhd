--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : ${entityname}_tb.vhd
-- Author   : ${author}
-- Date     : ${date}
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   ${date}  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity ${entityname}_tb is
    ${tbgenerics}
end ${entityname}_tb;

architecture testbench of ${entityname}_tb is

    constant CLK_PERIOD : time := 10 ns;

    ${signalsdeclaration}
    signal sim_end_s : boolean := false;

    ${componentdeclaration}

begin

    ${componentinstantiation}

    clk_proc : process is
    begin
        ${tbclock} <= '0';
        wait for CLK_PERIOD / 2;
        ${tbclock} <= '1';
        wait for CLK_PERIOD / 2;
        if sim_end_s then
            wait;
        end if;
    end process; -- clk_proc

    rst_proc : process is
    begin
        ${tbreset} <= '1';
        wait for 2 * CLK_PERIOD;
        ${tbreset} <= '0';
        wait;
    end process; -- rst_proc

    stimulus_proc: process is
    begin
        ${defaultstimuli}

        -- do something
        case TESTCASE is
            when 0      => -- default testcase
            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
