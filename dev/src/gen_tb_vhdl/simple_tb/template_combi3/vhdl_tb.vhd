--------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
--------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
--------------------------------------------------------------------------------
--
-- File     : ${entityname}_tb.vhd
-- Author   : ${author}
-- Date     : ${date}
--
-- Context  :
--
--------------------------------------------------------------------------------
-- Description : This module is a simple VHDL testbench.
--               It instanciates the DUV and proposes a TESTCASE generic to
--               select which test to start.
--
--------------------------------------------------------------------------------
-- Dependencies : -
--
--------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Person     Comments
-- 0.1   ${date}  TbGen      Initial version
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity ${entityname}_tb is
    ${tbgenerics}
end ${entityname}_tb;

architecture testbench of ${entityname}_tb is

    ${portsrecordtypes}

    signal stimulus_sti  : stimulus_t;
    signal observed_obs  : observed_t;
    signal reference_ref : observed_t;

    constant PERIOD : time := 10 ns;

    signal sim_end_s : boolean   := false;
    signal synchro_s : std_logic := '0';

    ${componentdeclaration}

    procedure check(stimulus  : stimulus_t;
                    observed  : observed_t;
                    reference : observed_t) is
    begin
        -- TODO : do the check, maybe differently
        if (observed /= reference) then
            report "Error in check" severity error;
        end if;
    end check;

    procedure calculate_reference(stimulus : stimulus_t;
                                  reference : out observed_t) is
    begin
        -- TODO : calculate the reference

    end calculate_reference;


    procedure testcase0(signal synchro : in std_logic;
                        signal stimulus : out stimulus_t;
                        signal reference : out observed_t) is

        variable stimulus_v  : stimulus_t;
        variable reference_v : observed_t;

    begin
        for i in 0 to 999 loop
            wait until rising_edge(synchro);
            -- TODO : assign the stimulus_v variable
            ${defaultstimulirecordvariable}

            stimulus <= stimulus_v;
            calculate_reference(stimulus_v, reference_v);
            reference <= reference_v;
        end loop;
    end testcase0;

begin

    ${componentinstantiationrecord}

    synchro_proc : process is
    begin
        while not(sim_end_s) loop
            synchro_s <= '0', '1' after PERIOD/2;
            wait for PERIOD;
        end loop;
        wait;
    end process;

    verif_proc : process is
    begin
        loop
            wait until falling_edge(synchro_s);
            check(stimulus_sti, observed_obs, reference_ref);
        end loop;
    end process;

    stimulus_proc: process is
    begin
        ${defaultstimulirecord}

        report "Running TESTCASE " & integer'image(TESTCASE) severity note;

        -- do something
        case TESTCASE is
            when 0      => -- default testcase

                testcase0(synchro_s, stimulus_sti, reference_ref);

            when others => report "Unsupported testcase : "
                                  & integer'image(TESTCASE)
                                  severity error;
        end case;

        -- end of simulation
        sim_end_s <= true;

        -- stop the process
        wait;

    end process; -- stimulus_proc

end testbench;
