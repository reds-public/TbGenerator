
import getopt
import sys
import os
import pprint
import time
from pathlib import Path

currentDir = os.path.dirname(__file__)
if currentDir == '':
    relativeDir = ''
else:
    relativeDir = currentDir + '/'

parserDir = relativeDir +'../../parsers'
sys.path.append(parserDir)
commonDir = relativeDir +'../../common'
sys.path.append(commonDir)

from vhdlentityparserlight import *
from common import *


# Check that we are using the correct Python version
if sys.version_info[0] < 3:
    raise "Must be using Python 3"


signal_suffix = {
    'IN' : '_sti',
    'OUT' : '_obs',
    'INOUT' : '_stiobs'
}

port_suffix = {
    'IN' : '_i',
    'OUT' : '_o',
    'INOUT' : '_io'
}


def signalNameFromPort(port):
    portname = getPortName(port)
    signal_name = getPortName(port)
    if (portname.rfind(port_suffix[getPortDirection(port)]) == len(portname)-2):
        signal_name = portname[:-len(port_suffix[getPortDirection(port)])] + signal_suffix[getPortDirection(port)]
    return signal_name

def raw_signal_name(port):
    signal_name = port[0]
    if (port[0].rfind(port_suffix[port[2]]) == len(port[0])-2):
        signal_name = port[0].replace(port_suffix[port[2]], "")
    return signal_name

def addSpaces(nb):
    str = ''
    for i in range(0, nb):
        str += ' '
    return str

def generateAll(inputFileName, destDir, templateDir, overwrite):
    print('Generating TB structure')
    print('    File: {0}'.format(inputFileName))
    print('    Dest dir: {0}'.format(destDir))
    print('    Template: {0}'.format(templateDir))
    try:
        os.mkdir(destDir)
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir))

    try:
        os.mkdir(destDir + '/scripts')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/scripts'))

    try:
        os.mkdir(destDir + '/src_tb')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/src_tb'))

    try:
        os.mkdir(destDir + '/comp')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/comp'))

    Path(destDir + '/comp/.placeholder').touch()

    dir = os.path.dirname(__file__)
    realTemplateDir = os.path.join(dir, templateDir)
    print('    Real template: {0}'.format(realTemplateDir))

    entireFile = parseFile(inputFileName)
    entity = getEntity(entireFile)

#    pp = pprint.PrettyPrinter(depth=12)
#    pp.pprint(entity)



    entityRelativePath = os.path.relpath(inputFileName, destDir + '/scripts')
    entityDirReativePath = os.path.dirname(entityRelativePath)


    # TODO : Choose the best way to get the clock
    clock = 'clock_unfound'
    reset = 'reset_unfound'

    clockPort = getPorts(entity)[0]
    resetPort = getPorts(entity)[0]
    for p in getPorts(entity):
        if (isClock(p)):
            clock = getPortName(p)
            clockPort = p
        if (isReset(p)):
            reset = getPortName(p)
            resetPort = p

    tbclock = clock
    tbreset = signalNameFromPort(resetPort)

    tbsignalsdeclaration = ''
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        if (isClock(port)):
            tbsignalsdeclaration += '{0} {1} = 0;\n'.format(getPortType(port), getPortName(port))
        else:
            tbsignalsdeclaration += '{0} {1};\n'.format(getPortType(port), getPortName(port))


    tbgenerics =  'generic (\n'
    tbgenerics += '    TESTCASE : integer := 0'


    if (len(getGenerics(entity)) > 0):
        tbgenerics += ';\n' # End of TESTCASE
        sizestring = 0
        for port in (getGenerics(entity)):
            sizestring = max(sizestring, len(getGenericName(port)))
        sizestring = max(sizestring, len('TESTCASE'));
        for i in range(0, len(getGenerics(entity))):
            port = getGenerics(entity)[i]
            tbgenerics += '    ' + getGenericName(port) + addSpaces(sizestring - len(getGenericName(port))) + ' : ' \
                                    + getGenericType(port) \
                                    + ' := ' + getGenericValue(port)
            if (i != len(getGenerics(entity)) - 1):
                tbgenerics += ';'
            tbgenerics += '\n'
    else:
        tbgenerics += '\n' # End of TESTCASE
    tbgenerics += ');\n'


    sizestring = 0
    for port in (getPorts(entity)):
        sizestring = max(sizestring, len(signalNameFromPort(port)))

    signalsdeclaration = ''

    for p in getPorts(entity):
        signalsdeclaration += 'signal ' + signalNameFromPort(p) + addSpaces(sizestring - len(signalNameFromPort(p))) + ' : ' + getPortType(p) + ';\n'

    componentdeclaration = ''
    componentdeclaration += 'component ' + getName(entity) + ' is\n'
    if (len(getGenerics(entity)) > 0):
        componentdeclaration += 'generic (\n'
        sizestring = 0
        for port in (getGenerics(entity)):
            sizestring = max(sizestring, len(getGenericName(port)))

        for i in range(0, len(getGenerics(entity))):
            port = getGenerics(entity)[i]
            componentdeclaration += '    ' + getGenericName(port) + addSpaces(sizestring - len(getGenericName(port))) + ' : ' \
                                    + getGenericType(port) \
                                    + ' := ' + getGenericValue(port)
            if (i != len(getGenerics(entity)) - 1):
                componentdeclaration += ';'
            componentdeclaration += '\n'
        componentdeclaration += ');\n'

    componentdeclaration += 'port (\n'

    sizestring = 0
    for port in (getPorts(entity)):
        sizestring = max(sizestring, len(getPortName(port)))

    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        componentdeclaration += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' : ' + getVhdlPortDirection(port) \
                                + ' ' + getPortType(port)
        if (i != len(getPorts(entity)) - 1):
            componentdeclaration += ';'
        componentdeclaration += '\n'
    componentdeclaration += ');\n'
    componentdeclaration += 'end component;\n'

    portsrecordtypes = 'type stimulus_t is record\n'
    sizestring = 0
    for port in (getInputPorts(entity)):
        sizestring = max(sizestring, len(getPortName(port)))

    for i in range(0, len(getInputPorts(entity))):
        port = getInputPorts(entity)[i]
        portsrecordtypes += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' : ' \
                                + getPortType(port) + ';\n'
    portsrecordtypes += 'end record;\n\n'

    portsrecordtypes += 'type observed_t is record\n'
    sizestring = 0
    for port in (getOutputPorts(entity)):
        sizestring = max(sizestring, len(getPortName(port)))

    for i in range(0, len(getOutputPorts(entity))):
        port = getOutputPorts(entity)[i]
        portsrecordtypes += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' : ' \
                                + getPortType(port) + ';\n'
    portsrecordtypes += 'end record;\n'

    componentinstantiation = ''
    componentinstantiation += 'duv : ' + getName(entity) + '\n'


    if (len(getGenerics(entity)) > 0):
        componentinstantiation += 'generic map (\n'
        sizestring = 0
        for port in (getGenerics(entity)):
            sizestring = max(sizestring, len(getGenericName(port)))

        for i in range(0, len(getGenerics(entity))):
            port = getGenerics(entity)[i]
            componentinstantiation += '    ' + getGenericName(port) + addSpaces(sizestring - len(getGenericName(port))) + ' => ' \
                                      + getGenericName(port)
            if (i != len(getGenerics(entity)) - 1):
                componentinstantiation += ','
            componentinstantiation += '\n'
        componentinstantiation += ')\n'


    sizestring = 0
    for port in (getPorts(entity)):
        sizestring = max(sizestring, len(getPortName(port)))

    componentinstantiation += 'port map (\n'
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        componentinstantiation += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' => ' + signalNameFromPort(port)
        if (i != len(getPorts(entity)) - 1):
            componentinstantiation += ','
        componentinstantiation += '\n'

    componentinstantiation += ');\n'

    componentinstantiationrecord = ''
    componentinstantiationrecord += 'duv : ' + getName(entity) + '\n'

    if (len(getGenerics(entity)) > 0):
        componentinstantiationrecord += 'generic map (\n'
        sizestring = 0
        for port in (getGenerics(entity)):
            sizestring = max(sizestring, len(getGenericName(port)))

        for i in range(0, len(getGenerics(entity))):
            port = getGenerics(entity)[i]
            componentinstantiationrecord += '    ' + getGenericName(port) + addSpaces(sizestring - len(getGenericName(port))) + ' => ' \
                                      + getGenericName(port)
            if (i != len(getGenerics(entity)) - 1):
                componentinstantiationrecord += ','
            componentinstantiationrecord += '\n'
        componentinstantiationrecord += ')\n'

    sizestring = 0
    for port in (getPorts(entity)):
        sizestring = max(sizestring, len(getPortName(port)))

    componentinstantiationrecord += 'port map (\n'
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        if (isInputPort(port)):
            componentinstantiationrecord += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' => stimulus_sti.' + getPortName(port)
        else:
            componentinstantiationrecord += '    ' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' => observed_obs.' + getPortName(port)
        if (i != len(getPorts(entity)) - 1):
            componentinstantiationrecord += ','
        componentinstantiationrecord += '\n'

    componentinstantiationrecord += ');\n'


    sizestring = 0
    for port in (getPorts(entity)):
        sizestring = max(sizestring, len(signalNameFromPort(port)))

    defaultstimuli = ''
    for port in getPorts(entity):
        if (not isOutputPort(port)):
            defaultstimuli +='-- ' + signalNameFromPort(port) + addSpaces(sizestring - len(signalNameFromPort(port))) + ' <= default_value;\n'


    sizestring = 0
    for port in (getPorts(entity)):
        if (not isOutputPort(port)):
            sizestring = max(sizestring, len(getPortName(port)))

    defaultstimulirecord = ''
    for port in getPorts(entity):
        if (not isOutputPort(port)):
            defaultstimulirecord +='-- stimulus_sti.' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' <= default_value;\n'

    defaultstimulirecordvariable = ''
    for port in getPorts(entity):
        if (not isOutputPort(port)):
            defaultstimulirecordvariable +='-- stimulus_v.' + getPortName(port) + addSpaces(sizestring - len(getPortName(port))) + ' := default_value;\n'

    defaultGenerics = ''
    for gen in getGenerics(entity):
        defaultGenerics += getGenericValue(gen) + ' '

    genericArguments = ''
    for i in range(0, len(getGenerics(entity))):
        genericArguments += '$' + str(i+2) + ' '

    replacements = {'entityname' : getName(entity),
            'author' : 'TbGenerator',
            'date' : time.strftime("%d.%m.%Y"),
            'entitydirpath' : entityDirReativePath,
            'tbgenerics' : tbgenerics,
            'signalsdeclaration' : signalsdeclaration,
            'componentdeclaration' : componentdeclaration,
            'componentinstantiation' : componentinstantiation,
            'duvfilerelativepath' : entityRelativePath,
            'defaultstimuli' : defaultstimuli,
            'tbclock' : tbclock,
            'tbreset' : tbreset,
            'genericstcllist' : getGenericsTclList(entity),
            'genericstcldollarlist' : getGenericsTclDollarList(entity),
            'genericstclgenericlist' : getGenericsTclGenericList(entity),
            'defaultgenerics' : defaultGenerics,
            'genericarguments' : genericArguments,
            'portsrecordtypes' : portsrecordtypes,
            'componentinstantiationrecord' : componentinstantiationrecord,
            'defaultstimulirecord' : defaultstimulirecord,
            'defaultstimulirecordvariable' : defaultstimulirecordvariable
            }


    generateFile(realTemplateDir + '/assertions.psl',
                 destDir+'/src_tb/{0}_assertions.psl'.format(getName(entity)),
                 replacements, overwrite)


    generateFile(realTemplateDir + '/check.do',
                 destDir+'/scripts/check.do',
                 replacements, overwrite)


def printhelp():
    print ('gen_simple_vhdl_tb.py -i <inputfile> -d <destdir> -t <templatedir>')
    print ('    -h will show this help')
    print ('    -f forces overwrite of existing files. Use with care.')

def main(argv):
    inputFileName = ''
    destDir    = './'
    templateDir = 'template1'
    overwrite = False

    try:
       opts, args = getopt.getopt(argv,"hfi:t:d:",["ifile","templatedir","destdir"])
    except getopt.GetoptError:
        printhelp()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printhelp()
            sys.exit()
        elif opt in ("-f"):
            overwrite = True
        elif opt in ("-i", "--ifile"):
            inputFileName = arg
        elif opt in ("-d", "--destdir"):
            destDir = arg
        elif opt in ("-t", "--templatedir"):
            templateDir = arg

    generateAll(inputFileName, destDir, templateDir, overwrite)

if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])
