
import getopt
import sys
import os
import pprint
import time
from pathlib import Path

parserDir = os.path.dirname(__file__)+'/../../parsers'
sys.path.append(parserDir)
commonDir = os.path.dirname(__file__)+'/../../common'
sys.path.append(commonDir)

from vhdlentityparser import *
from common import *


# Check that we are using the correct Python version
if sys.version_info[0] < 3:
    raise "Must be using Python 3"


def generateAll(inputFileName, destDir, templateDir, overwrite):
    print('Generating TB structure')
    print('    File: {0}'.format(inputFileName))
    print('    Dest dir: {0}'.format(destDir))
    print('    Template: {0}'.format(templateDir))
    try:
        os.mkdir(destDir)
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir))

    try:
        os.mkdir(destDir + '/scripts')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/scripts'))

    try:
        os.mkdir(destDir + '/src_tb')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/src_tb'))

    try:
        os.mkdir(destDir + '/comp')
    except FileExistsError:
        None
        #print("The folder {0} already exists".format(destDir + '/comp'))

    Path(destDir + '/comp/.placeholder').touch()

    dir = os.path.dirname(__file__)
    realTemplateDir = os.path.join(dir, templateDir)
    print('    Real template: {0}'.format(realTemplateDir))

    entireFile = parseFile(inputFileName)
    entity = getEntity(entireFile)

#    pp = pprint.PrettyPrinter(depth=12)
#    pp.pprint(entity)

    portlist = ''
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        portlist += '{0} {1} {2}'.format(getSvPortDirection(port), getSvPortType(port), getPortName(port))
        if (not (i == len(getPorts(entity)) - 1)):
            portlist += ','
            portlist += '\n'


    entityRelativePath = os.path.relpath(inputFileName, destDir + '/scripts')

    # TODO : Choose the best way to get the clock
    clock = 'TODO__clock_notfound'
    reset = 'TODO__reset_notfound'

    for p in getPorts(entity):
        if (isClock(p)):
            clock = getPortName(p)
        if (isReset(p)):
            reset = getPortName(p)


    tbsignalsdeclaration = ''
    for i in range(0, len(getPorts(entity))):
        port = getPorts(entity)[i]
        if (isClock(port)):
            tbsignalsdeclaration += '{0} {1} = 0;\n'.format(getSvPortType(port), getPortName(port))
        else:
            tbsignalsdeclaration += '{0} {1};\n'.format(getSvPortType(port), getPortName(port))

    clockingblock = ''
    clockingblock += 'default clocking cb @(posedge ' + clock + ');\n'
    clockingblock += '    output #3ns ' + reset + ',\n'
    for i in range(0, len(getInputPorts(entity))):
        port = getInputPorts(entity)[i]
        if (not (isClock(port) or isReset(port))):
            clockingblock += '           ' + getPortName(port) + ' = ' + getPortName(port);
            if (i == len(getInputPorts(entity)) - 1):
                clockingblock += ';\n'
            else:
                clockingblock += ',\n'

    clockingblock += '    input \n'
    for i in range(0, len(getOutputPorts(entity))):
        port = getOutputPorts(entity)[i]
        clockingblock += '           ' + getPortName(port) + ' = ' + getPortName(port);
        if (i == len(getOutputPorts(entity)) - 1):
            clockingblock += ';\n'
        else:
            clockingblock += ',\n'#                output #3ns rst,
#                       a            = input_itf.a,
#                       b            = input_itf.b,
#                       c            = input_itf.c,
#                       input_valid  = input_itf.valid,
#                       output_ready = output_itf.ready;
#                input  input_ready  = input_itf.ready,
#                       result       = output_itf.result,
#                       output_valid = output_itf.valid;
    clockingblock += 'endclocking\n'


    defaultGenerics = ''
    for gen in getGenerics(entity):
        defaultGenerics += str(getGenericValue(gen)) + ' '

    genericArguments = ''
    for i in range(0, len(getGenerics(entity))):
        genericArguments += '$' + str(i+2) + ' '

    replacements = {'entityname' : getName(entity),
            'author' : 'TbGenerator',
            'date' : time.strftime("%d.%m.%Y"),
            'generics' : getSvGenerics(entity),
            'genericswithtestcase' : getSvGenericsWithTestCase(entity),
            'portlist' : portlist,
            'genericimpl' : getSvGenericsShort(entity),
            'defaultgenerics' : defaultGenerics,
            'genericarguments' : genericArguments,
            'duvfilerelativepath' : entityRelativePath,
            'clock' : clock,
            'reset' : reset,
            'tbsignalsdeclaration' : tbsignalsdeclaration,
            'clockingblock' : clockingblock,
            'genericstcllist' : getGenericsTclList(entity),
            'genericstcldollarlist' : getGenericsTclDollarList(entity),
            'genericstclgenericlist' : getGenericsTclGenericList(entity)
            }


    generateFile(realTemplateDir + '/sv_tb.sv',
                 destDir+'/src_tb/{0}_tb.sv'.format(getName(entity)),
                 replacements, overwrite)


    generateFile(realTemplateDir + '/sim.do',
                 destDir+'/scripts/sim.do',
                 replacements, overwrite)


def printhelp():
    print ('gen_simple_sv_tb.py -i <inputfile> -d <destdir> -t <templatedir>')
    print ('    -h will show this help')
    print ('    -f forces overwrite of existing files. Use with care.')

def main(argv):
    inputFileName = ''
    destDir    = './'
    templateDir = 'template1'
    overwrite = False

    try:
       opts, args = getopt.getopt(argv,"hfi:t:d:",["ifile","templatedir","destdir"])
    except getopt.GetoptError:
        printhelp()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printhelp()
            sys.exit()
        elif opt in ("-f"):
            overwrite = True
        elif opt in ("-i", "--ifile"):
            inputFileName = arg
        elif opt in ("-d", "--destdir"):
            destDir = arg
        elif opt in ("-t", "--templatedir"):
            templateDir = arg

    generateAll(inputFileName, destDir, templateDir, overwrite)


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv[1:])
